package model;


import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class DocumentAttributes {
    private final String[] givenNames;
    private final String maritalName;
    private final String surname;
    private final String dateOfBirth;
    private final String gender;
    private final String dateOfChange;
    private final String place;
    private final String documentFrontPhoto;
    private final String documentBackPhoto;
    private final String mrz;
}
