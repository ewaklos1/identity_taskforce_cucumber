package ipv.tests.api;

import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ipv.tests.testdata.EnvironmentProperties;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.Base64Utils;
import org.springframework.util.StreamUtils;

import static io.restassured.http.ContentType.JSON;
import static ipv.tests.testdata.EnvironmentProperties.GIPS_BASE_PATH;
import static ipv.tests.utils.RequestUtils.*;
import static ipv.tests.utils.SessionVariables.RESPONSE;

public class GipsRsApi {

    public static Response createIdentity(String key, String value) {
        Response resp = getBaseRequestSpecification()
                .header(createXForwarderForHeader("to-specify"))
                .header(createTenantIdHeader(EnvironmentProperties.TENANT_ID))
                .header(createTenantRoleHeader(EnvironmentProperties.TENANT_ROLE))
                .multiPart(new MultiPartSpecBuilder(buildCreateIdentityRequestBody(key, value))
                        .with().controlName("context")
                        .emptyFileName().and().mimeType(ContentType.JSON.toString()).build())
                .when()
                .post(GIPS_BASE_PATH + "/identities")
                .prettyPeek();
        Serenity.setSessionVariable(RESPONSE).to(resp);
        return resp;
    }

    private static JSONArray buildCreateIdentityRequestBody(String key, String value) {
        JSONArray object = new JSONArray();
        JSONObject context = new JSONObject();
        context.put("key", key);
        context.put("value", value);
        object.add(context);
        Serenity.recordReportData().withTitle("identity context from request")
                .andContents(object.toJSONString());
        return object;
    }

    public static Response createPassport(String frontDocumentPath, String identityId) {
                Response resp = getBaseRequestSpecification()
                        .header(createXForwarderForHeader("to-specify"))
                        .header(createTenantIdHeader(EnvironmentProperties.TENANT_ID))
                        .header(createTenantRoleHeader(EnvironmentProperties.TENANT_ROLE))
                .multiPart(new MultiPartSpecBuilder(getImage(frontDocumentPath)).mimeType("image/jpeg")
                        .with().controlName("DocumentFront").and().emptyFileName().and()
                        .build())
                .when()
                .post(GIPS_BASE_PATH + "/identities/" + identityId + "/id-documents/capture")
                .prettyPeek();
        Serenity.setSessionVariable(RESPONSE).to(resp);
        return resp;
    }

    @SneakyThrows
    private static String getImage(String path) {
        ClassPathResource imgFile = new ClassPathResource(path);
        byte[] bytes =  StreamUtils.copyToByteArray(imgFile.getInputStream());
        Serenity.recordReportData().withTitle("image sent in request from request")
                .andContents(Base64Utils.encodeToString(bytes));
        return Base64Utils.encodeToString(bytes);
    }

    public static Response deletePassport(String identityId, String evidenceId) {
        Response resp = getBaseRequestSpecification()
                .header(createXForwarderForHeader("to-specify"))
                .header(createTenantIdHeader(EnvironmentProperties.TENANT_ID))
                .header(createTenantRoleHeader(EnvironmentProperties.TENANT_ROLE))
                .contentType(JSON)
                .when()
                .delete(GIPS_BASE_PATH + "/identities/" + identityId + "/id-documents/" + evidenceId)
                .prettyPeek();
        Serenity.setSessionVariable(RESPONSE).to(resp);
        return resp;
    }

    public static Response getIdentity(String identityId) {
        Response resp = getBaseRequestSpecification()
                .header(createXForwarderForHeader("to-specify"))
                .contentType(JSON)
                .when()
                .get(GIPS_BASE_PATH + "/identities/" + identityId )
                .prettyPeek();
        Serenity.setSessionVariable(RESPONSE).to(resp);
        return resp;
    }

    public static Response getIdentityStatus(String identityId) {
        Response resp = getBaseRequestSpecification()
                .header(createXForwarderForHeader("to-specify"))
                .header(createTenantIdHeader(EnvironmentProperties.TENANT_ID))
                .header(createTenantRoleHeader(EnvironmentProperties.TENANT_ROLE))
                .contentType(JSON)
                .when()
                .get(GIPS_BASE_PATH + "/identities/" + identityId + "/status")
                .prettyPeek();
        Serenity.setSessionVariable(RESPONSE).to(resp);
        return resp;
    }
}
