package ipv.tests.api;

import io.restassured.response.Response;
import ipv.tests.testdata.EnvironmentProperties;
import ipv.tests.utils.RequestUtils;
import ipv.tests.utils.SessionVariables;
import model.DocumentAttributes;
import net.serenitybdd.core.Serenity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Collections;

import static io.restassured.http.ContentType.JSON;

public class SisRsApi {

    public static final String LOA_LEVEL = "2";

    public static final String TRUSTED_FAN_BP_ID="TRUSTED_FAN_10";
    public static final String TRUSTED_FAN_BP_NAME="TRUSTED_FAN";
    public static final String DELETE_PASSPORT_BP_ID="passport_ok";
    public static final String LOA2_BP_ID="LOA2PSI";

    public static Response getTransactionDetails(String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .get(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId)
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    public static Response createTransaction(String bpName) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .body(buildCreateTransactionRequestBody(bpName))
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH  + "/transaction/create")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.TRANSACTION_ID).to(resp.body().jsonPath().get("identifier"));
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildCreateTransactionRequestBody(String bpName) {
        JSONObject body = new JSONObject();
        body.put("businessId", TRUSTED_FAN_BP_ID);
        body.put("loa", LOA_LEVEL);
        JSONObject dictionary = new JSONObject();
        JSONArray element = new JSONArray();
        JSONObject businessProcess = new JSONObject();
        businessProcess.put("value", bpName);
        businessProcess.put("name", "bp");
        element.add(businessProcess);
        dictionary.put("element", Collections.singletonList(element).get(0));
        body.put("dictionary", dictionary);
        body.put("source", "ANDROID");
        body.put("action", "IDENTITY_REGISTRATION");
        return body;
    }

    public static Response submitPhone(String phoneNumber, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildSubmitPhoneRequestBody(phoneNumber))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/phone-channel")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    public static JSONObject buildSubmitPhoneRequestBody(String phoneNumber) {
        JSONObject body = new JSONObject();
        body.put("identifier", phoneNumber);
        return body;
    }

    public static Response submitEmail(String email, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildSubmitEmailRequestBody(email))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/email-channel")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    public static JSONObject buildSubmitEmailRequestBody(String email) {
        JSONObject body = new JSONObject();
        body.put("identifier", email);
        return body;
    }

    public static Response phoneBinding(String phoneNumber, String otp) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildPhoneBindingRequestBody(phoneNumber, otp))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + "phone-binding")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildPhoneBindingRequestBody(String phoneNumber, String otp) {
        JSONObject body = new JSONObject();
        body.put("identifier", phoneNumber);
        body.put("otpValue", otp);
        return body;
    }

    public static Response prepareLicense(String serialNumber, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .body(buildPrepareLicenseRequestBody(serialNumber))
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/prepare-license")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildPrepareLicenseRequestBody(String serialNumber) {
        JSONObject body = new JSONObject();
        body.put("serialNumber", serialNumber);
        return body;
    }

    public static Response submitDocument(DocumentAttributes documentAttributes, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildDocumentPhotoRequest(documentAttributes))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/iddocument")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    public static JSONArray buildDocumentPhotoRequest(DocumentAttributes documentAttributes) {
        JSONArray body = new JSONArray();
        JSONObject frontPicture = new JSONObject();
        frontPicture.put("side", "FRONT");
        frontPicture.put("mrz", "");
        frontPicture.put("source", "PICTURE");
        frontPicture.put("data", documentAttributes.getDocumentFrontPhoto());
        body.add(frontPicture);
        JSONObject backPicture = new JSONObject();
        backPicture.put("side", "BACK");
        backPicture.put("mrz", "");
        backPicture.put("source", "PICTURE");
        backPicture.put("data", documentAttributes.getDocumentBackPhoto());
        body.add(backPicture);
        return body;
    }

    public static Response faceMatching(String selfie, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildFaceMatchingRequestBody(selfie))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/faceMatching")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildFaceMatchingRequestBody(String selfie) {
        JSONObject body = new JSONObject();
        body.put("contentType", "JPEG");
        body.put("value", selfie);
        return body;
    }

    public static Response submitSelfie(String selfie, String identityId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .body(buildFaceMatchingRequestBody(selfie))
                .contentType(JSON)
                .expect().statusCode(200)
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/identities/" + identityId + "attributes/portrait/capture")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    public static Response submitAdditionalAttributes(DocumentAttributes documentAttributes, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .body(buildAdditionalAttributesRequestBody(documentAttributes))
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/additionalAttributes")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildAdditionalAttributesRequestBody(DocumentAttributes documentAttributes) {
        JSONObject body = new JSONObject();
        JSONArray additionalAttributes = new JSONArray();
        JSONObject attribute = new JSONObject();
        attribute.put("name", "place");
        attribute.put("value", documentAttributes.getPlace());
        additionalAttributes.add(attribute);
        body.put("additionalAttributes", additionalAttributes);
        return body;
    }

    public static Response endRegistration(String uuid, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .body(buildEndTransactionRequestBody(uuid))
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/end")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject buildEndTransactionRequestBody(String uuid) {
        JSONObject body = new JSONObject();
        body.put("uuid", uuid);
        return body;
    }

    public static Response issueMid(String encrypted, String transactionId) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .body(createEncryptedBlock(encrypted))
                .when()
                .post(EnvironmentProperties.GIPS_BASE_PATH + "/transactions/" + transactionId + "/issuance")
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }

    private static JSONObject createEncryptedBlock(String encryptedBlock) {
        JSONObject body = new JSONObject();
        body.put("encrypted", encryptedBlock);
        return body;
    }

    public static Response getMobileId(String mobileIdUrl, String mobileIdToken) {
        Response resp = RequestUtils.getBaseRequestSpecification()
                .contentType(JSON)
                .header(RequestUtils.createBearerAuthorizationHeader(mobileIdToken))
                .expect().statusCode(200)
                .when()
                .get(EnvironmentProperties.GIPS_BASE_PATH  + mobileIdUrl)
                .prettyPeek();
        Serenity.setSessionVariable(SessionVariables.RESPONSE).to(resp);
        return resp;
    }
}
