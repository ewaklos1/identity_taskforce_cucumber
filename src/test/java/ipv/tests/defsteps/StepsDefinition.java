package ipv.tests.defsteps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import ipv.tests.api.GipsRsApi;
import ipv.tests.api.SisRsApi;
import ipv.tests.steps.CommonSteps;
import ipv.tests.testdata.TestData;
import ipv.tests.testdata.TestImages;
import ipv.tests.utils.SessionVariables;
import ipv.tests.utils.TransactionStateUtils;
import net.serenitybdd.core.Serenity;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import java.net.HttpURLConnection;

import static org.assertj.core.api.Assertions.assertThat;

public class StepsDefinition extends ScenarioSteps {

    @When("create transaction for {word} business process")
    public void createTransaction(String bpName) {
        SisRsApi.createTransaction(bpName);
        CommonSteps.buildDefaultDocumentAttributesObject();

    }

    @Given("Identity with passport exists")
    public void createIdentityWithPassport() {
        createIdentity();
        identityCreated();
        createPassport();
        passportCaptured();
    }

    @When("submit email")
    public void submitEmail() {
        SisRsApi.submitEmail(TestData.EMAIL, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit phone number")
    public void submitPhoneNumber() {
        SisRsApi.submitPhone(TestData.PHONE_NUMBER, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit phone otp")
    public void submitPhoneOtp() {
        SisRsApi.phoneBinding(TestData.PHONE_NUMBER, TestData.OTP);
    }

    @When("submit license")
    public void submitLicence() {
        SisRsApi.prepareLicense(TestData.SERIAL_NUMBER, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit document")
    public void submitDocument() {
        SisRsApi.submitDocument(Serenity.sessionVariableCalled(SessionVariables.DOCUMENT_ATTRIBUTES), Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit face match")
    public void submitFace() {
        SisRsApi.faceMatching(TestImages.SELFIE, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit selfie")
    public void submitSelfie() {
        SisRsApi.submitSelfie(TestImages.SELFIE, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit identity attributes")
    public void submitIdentityAttributes() {
        throw new io.cucumber.java.PendingException();
    }

    @Then("identity score has been calculated")
    public void identityScoreWasCalculated() {
        throw new io.cucumber.java.PendingException();
    }

    @Then("identity contains declared attributes")
    public void identityContainsDeclaredAttributes() {
        throw new io.cucumber.java.PendingException();
    }

    @When("submit additional attributes")
    public void submitAdditionalAttributes() {
        SisRsApi.submitAdditionalAttributes(Serenity.sessionVariableCalled(SessionVariables.DOCUMENT_ATTRIBUTES), Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit issuance")
    public void issueMid() {
        SisRsApi.issueMid(TestData.ENCRYPTED, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("submit end registration")
    public void endRegistration() {
        SisRsApi.endRegistration(TestData.UUID, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("get transaction details")
    public void whenIdWayRequestsTransactionDetails() {
        SisRsApi.getTransactionDetails(Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @When("retrieve mID")
    public void getMobileId() {
        SisRsApi.getMobileId(Serenity.sessionVariableCalled(SessionVariables.MOBILE_ID_URL), Serenity.sessionVariableCalled(SessionVariables.MOBILE_ID_TOKEN));
    }

    @When("create identity")
    public void createIdentity() {
        GipsRsApi.createIdentity("BUSINESS_ID", SisRsApi.DELETE_PASSPORT_BP_ID);
    }

    @Then("valid response sent and identity created")
    public void identityCreated() {
        Response response = Serenity.sessionVariableCalled(SessionVariables.RESPONSE);
        assertThat(response.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_OK);
        Serenity.setSessionVariable(SessionVariables.IDENTITY_ID).to(response.body().jsonPath().get("id"));
    }

    @When("create passport capture")
    public void createPassport() {
        GipsRsApi.createPassport("images/cni-recto.jpg", Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID));
    }

    @Then("valid response sent and passport captured")
    public void passportCaptured() {
        Response response = Serenity.sessionVariableCalled(SessionVariables.RESPONSE);
        assertThat(response.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_OK);
        CommonSteps.waitForIdentityStatus("VERIFIED", "ATTRIBUTES", Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID));
        CommonSteps.waitForIdentityStatus("VERIFIED", "ATTRIBUTES", Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID));
        Serenity.setSessionVariable(SessionVariables.EVIDENCE_ID).to(response.body().jsonPath().get("id"));
    }

    @When("delete passport identity")
    public void deletePassport() {
        GipsRsApi.deletePassport(Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID), Serenity.sessionVariableCalled(SessionVariables.EVIDENCE_ID));
    }

    @Then("valid response sent and passport deleted")
    public void passportDeleted() {
        Response response = Serenity.sessionVariableCalled(SessionVariables.RESPONSE);
        assertThat(response.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_NO_CONTENT);
        response = GipsRsApi.getIdentity(Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID));
        assertThat(response.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_NOT_FOUND);
    }

    @When("get identity")
    public void getIdentity() {
        GipsRsApi.getIdentity(Serenity.sessionVariableCalled(SessionVariables.IDENTITY_ID));
    }

    @Then("{word} phase is ACCEPTED")
    public void stepIsAccepted(String step) {
        TransactionStateUtils.awaitAcceptance(step, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }

    @Then("valid response with mobileIdUrl and mobileIdToken is sent")
    public void thenIpvSendsMobileIdUrlAndMobileIdToken() {
        Response response = Serenity.sessionVariableCalled(SessionVariables.RESPONSE);
        CommonSteps.saveMobileTokenAndUrl(response);
    }

    @Then("global status is OK and {word} phase is ACCEPTED")
    public void globalStatusIsOk(String step) {
        TransactionStateUtils.awaitOK(step, Serenity.sessionVariableCalled(SessionVariables.TRANSACTION_ID));
    }


    @Then("mID contains declared attributes")
    public void mIdContainsDeclaredAttributes() {
        throw new io.cucumber.java.PendingException();
    }

}
