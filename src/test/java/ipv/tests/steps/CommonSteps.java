package ipv.tests.steps;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import ipv.tests.api.GipsRsApi;
import ipv.tests.testdata.TestData;
import ipv.tests.testdata.TestImages;
import ipv.tests.utils.SessionVariables;
import model.DocumentAttributes;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.awaitility.Awaitility;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonSteps {

    @Step
    public static void saveMobileTokenAndUrl(Response response){
        JsonPath json = response.body().jsonPath();
        String mobileIdUrl = json.getString("mobileIdUrl");
        String mobileIdToken = json.getString("mobileIdToken");
        assertThat(mobileIdUrl).isNotEmpty();
        Serenity.setSessionVariable(SessionVariables.MOBILE_ID_URL).to(mobileIdUrl);
        assertThat(mobileIdToken).isNotEmpty();
        Serenity.setSessionVariable(SessionVariables.MOBILE_ID_TOKEN).to(mobileIdToken);
    }

    @Step
    public static void waitForIdentityStatus(String status, String type, String identityId){
        GipsRsApi.getIdentityStatus(identityId).body().jsonPath().get("singleStatuses[0].status").equals("VERIFIED");
        Awaitility.await()
                .atMost(Duration.ofSeconds(10))
                .until(() -> GipsRsApi.getIdentityStatus(identityId).body().jsonPath().get("singleStatuses[0].status").equals("VERIFIED"));
    }

    @Step
    public static void buildDefaultDocumentAttributesObject(){
        DocumentAttributes documentAttributes = DocumentAttributes.builder()
                .givenNames(TestData.GIVEN_NAMES)
                .surname(TestData.SURNAME)
                .dateOfBirth(TestData.DATE_OF_BIRTH)
                .gender(TestData.GENDER)
                .dateOfChange(TestData.DATE_OF_CHANGE)
                .documentFrontPhoto(TestImages.FRONT_DOCUMENT)
                .documentBackPhoto(TestImages.BACK_DOCUMENT)
                .maritalName(TestData.SURNAME)
                .place(TestData.PLACE)
                .build();
        Serenity.setSessionVariable(SessionVariables.DOCUMENT_ATTRIBUTES).to(documentAttributes);
    }

}