package ipv.tests.testdata;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import static java.util.Optional.ofNullable;

public class EnvironmentProperties {

    private static Properties props = new Properties();

    static {
        try (
                InputStream fis = ofNullable(System.getProperty("config")).map(s -> {
                    try {
                        return (InputStream) new FileInputStream(s);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException("Missing file with environment configuration");
                    }
                })
                        .orElseGet(() -> EnvironmentProperties.class
                                .getClassLoader()
                                .getResourceAsStream("environment.properties"))
        ) {
            props.load(fis);
        } catch (Exception e) {
            throw new RuntimeException("Missing file with environment configuration");
        }
    }

    public static final String GIPS_BASE_PATH = props.getProperty("gips.url");
    public static final String API_KEY = props.getProperty("api.key");
    public static final String TENANT_ID = props.getProperty("tenant.id");
    public static final String TENANT_ROLE = props.getProperty("tenant.role");
}
