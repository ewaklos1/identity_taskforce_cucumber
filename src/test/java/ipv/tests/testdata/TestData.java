package ipv.tests.testdata;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import static java.util.Optional.ofNullable;

public class TestData {

    private static Properties props = new Properties();

    static {
        try (
                InputStream fis = ofNullable(System.getProperty("testData")).map(s -> {
                    try {
                        return (InputStream) new FileInputStream(s);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException("Missing file with test data configuration");
                    }
                })
                        .orElseGet(() -> TestData.class
                                .getClassLoader()
                                .getResourceAsStream("test_data.properties"))
        ) {
            props.load(fis);
        } catch (Exception e) {
            throw new RuntimeException("Missing file with test data configuration");
        }
    }

    public static final String OTP = props.getProperty("otp.value");
    public static final String EMAIL = props.getProperty("email");
    public static final String PHONE_NUMBER = props.getProperty("phone.number");
    public static final String SERIAL_NUMBER = props.getProperty("serial.number");
    public static final String MRZ = props.getProperty("mrz");
    public static final String PLACE = props.getProperty("place");
    public static final String UUID = props.getProperty("uuid");
    public static final String ENCRYPTED = props.getProperty("encrypted");
    public static final String JURISDICTION_ID = props.getProperty("jurisdiction.id");
    public static final String DOCUMENT_TYPE = props.getProperty("document.type");

    public static final String[] GIVEN_NAMES = props.getProperty("given.names").split(",");
    public static final String SURNAME = props.getProperty("surname");
    public static final String DATE_OF_BIRTH = props.getProperty("date.of.birth");
    public static final String DATE_OF_CHANGE = props.getProperty("date.of.change");
    public static final String PLACE_OF_BIRTH = props.getProperty("place.of.birth");
    public static final String NATIONALITY = props.getProperty("nationality");
    public static final String GENDER = props.getProperty("gender");
}
