package ipv.tests.testdata;

import lombok.Getter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import static java.util.Optional.ofNullable;

@Getter
public class TestImages {

    private static Properties props = new Properties();

    static {
        try (
                InputStream fis = ofNullable(System.getProperty("testImages")).map(s -> {
                    try {
                        return (InputStream) new FileInputStream(s);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException("Missing file with test images configuration");
                    }
                })
                        .orElseGet(() -> TestImages.class
                                .getClassLoader()
                                .getResourceAsStream("test_images.properties"))
        ) {
            props.load(fis);
        } catch (Exception e) {
            throw new RuntimeException("Missing file with test images configuration");
        }
    }
    public static final String FRONT_DOCUMENT = props.getProperty("frontDocument");
    public static final String BACK_DOCUMENT = props.getProperty("backDocument");
    public static final String SELFIE = props.getProperty("selfie");
}
