package ipv.tests.utils;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import net.jodah.failsafe.function.CheckedSupplier;
import net.thucydides.core.annotations.Step;

import java.net.HttpURLConnection;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static io.restassured.http.ContentType.JSON;
import static ipv.tests.testdata.EnvironmentProperties.GIPS_BASE_PATH;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class IdentityStateUtils {

    private static final Map<String, BiConsumer<Response, String>> STATUS_VERIFICATION_MAP = new HashMap<>();
    private static final Map<String, BiConsumer<Response, String>> ENCRYPTED_STATUS_VERIFICATION_MAP = new HashMap<>();
    private static final int INITIAL_DELAY = 2000;
    private static final int MAX_DELAY = 10000;
    private static final int MAX_DURATION = 240000;

    private static final String REJECTED_STATUS = "REJECTED";
    private static final String PROCESSING_STATUS = "PROCESSING";
    private static final String EXPECTING_INPUT_STATUS = "EXPECTING_INPUT";
    private static final String ACCEPTED_STATUS = "ACCEPTED";
    private static final String NOT_SET_STATUS = "NOT_SET";
    private static final String KO_STATUS = "KO";
    private static final String OK_STATUS = "OK";

    static {
        STATUS_VERIFICATION_MAP.put(ACCEPTED_STATUS, (statusResponse, stateName) ->
                verifyAndExtractState(statusResponse, stateName, EXPECTING_INPUT_STATUS, ACCEPTED_STATUS));
        STATUS_VERIFICATION_MAP.put(NOT_SET_STATUS, (statusResponse, stateName) ->
                verifyAndExtractState(statusResponse, stateName, EXPECTING_INPUT_STATUS, NOT_SET_STATUS));
        STATUS_VERIFICATION_MAP.put(REJECTED_STATUS, (statusResponse, stateName) ->
                verifyAndExtractState(statusResponse, stateName, EXPECTING_INPUT_STATUS, REJECTED_STATUS));
        STATUS_VERIFICATION_MAP.put(OK_STATUS, (statusResponse, stateName) ->
                verifyAndExtractState(statusResponse, stateName, OK_STATUS, ACCEPTED_STATUS));
        STATUS_VERIFICATION_MAP.put(KO_STATUS, (statusResponse, stateName) ->
                verifyAndExtractState(statusResponse, stateName, KO_STATUS, REJECTED_STATUS));
    }

    private static void verifyAndExtractState(Response statusResponse,
                                              String stateName,
                                              String expectingInputStatus,
                                              String acceptedStatus) {
        assertThat(statusResponse.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_OK);
        String globalStatus = getGlobalStatus(statusResponse);
        assertThat(globalStatus).isEqualTo(expectingInputStatus);
        String state = getState(statusResponse, stateName);
        assertThat(state).isEqualTo(acceptedStatus);
    }

    private static String getState(Response response, String stateName) {
        return (String) response.body().jsonPath().getMap("states").get(stateName);
    }

    private static String getGlobalStatus(Response response) {
        return response.body().jsonPath().getString("global");
    }

    @Step
    public static void awaitAcceptance(String stateName, String transactionId) {
        Response response = awaitStatusAction(transactionId, isRetryRequired());
        STATUS_VERIFICATION_MAP.get(ACCEPTED_STATUS).accept(response, stateName);
    }

    @Step
    public static void awaitOK(String stateName, String transactionId) {
        Response response = awaitStatusAction(transactionId, isRetryRequired());
        STATUS_VERIFICATION_MAP.get(OK_STATUS).accept(response, stateName);
    }

    @Step
    public static void awaitKO(String stateName, String transactionId) {
        Response response = awaitStatusAction(transactionId, isRetryRequired());
        STATUS_VERIFICATION_MAP.get(KO_STATUS).accept(response, stateName);
    }

    @Step
    public static void awaitNotSet(String stateName, String transactionId) {
        Response response = awaitStatusAction(transactionId, isRetryRequired());
        STATUS_VERIFICATION_MAP.get(NOT_SET_STATUS).accept(response, stateName);
    }

    @Step
    public static void awaitRejection(String stateName, String transactionId) {
        Response response = awaitStatusAction(transactionId, isRetryRequired());
        STATUS_VERIFICATION_MAP.get(REJECTED_STATUS).accept(response, stateName);
    }

    private static Response awaitStatusAction(String transactionId, Predicate<Response> isRetryRequired) {
        return awaitAction(getStatus(transactionId), isRetryRequired, INITIAL_DELAY, MAX_DELAY,
                MAX_DURATION);
    }

    private static <T> T awaitAction(CheckedSupplier<T> action, Predicate<T> retryPredicate, long initialDelay, long maxDelay, long maxDuration) {
        return Failsafe.with(new RetryPolicy<T>()
                .abortOn(Throwable.class)
                .handleResultIf(retryPredicate)
                .onSuccess(event -> log.info("Successfully executed"))
                .onAbort(event -> log.info("Execution aborted"))
                .onFailure(event -> log.info("Response is marked as failure"))
                .onRetriesExceeded(event -> log.info("Exceed maximum retrials count"))
                .onFailedAttempt(event -> log.info("Failed attempt"))
                .onRetry(event -> log.info("Retrying request"))
                .withMaxAttempts(100)
                .withMaxDuration(Duration.of(maxDuration, ChronoUnit.MILLIS))
                .withBackoff(initialDelay, maxDelay, ChronoUnit.MILLIS))
                .get(action);
    }


    private static CheckedSupplier<Response> getStatus(String identityId) {
        return () -> RestAssured.given().contentType(JSON)
                .relaxedHTTPSValidation().log().all()
                .expect().statusCode(200)
                .when()
                .get(GIPS_BASE_PATH + "/identities/" + identityId + "/status")
                .prettyPeek();
    }

    private static Predicate<Response> isRetryRequired() {
        return resp -> {
            assertThat(resp.getStatusCode()).isEqualTo(HttpURLConnection.HTTP_OK);
            JsonPath json = resp.body().jsonPath();
            return PROCESSING_STATUS.equals(json.getString("status"));
        };
    }

}
