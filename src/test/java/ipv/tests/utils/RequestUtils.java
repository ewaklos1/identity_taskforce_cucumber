package ipv.tests.utils;

import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;
import static net.serenitybdd.rest.SerenityRest.given;

import static io.restassured.http.ContentType.JSON;

public class RequestUtils {

    public static Header createBearerAuthorizationHeader(String accessToken) {
        return new Header("Authorization", "Bearer" + " " + accessToken);
    }

    public static Header createXForwarderForHeader(String value) {
        return new Header("X-Forwarded-For", value);
    }

    public static Header createApiKeyHeader(String apiKey) {
        return new Header("apikey", apiKey);
    }

    public static Header createTenantIdHeader(String tenantId) {
        return new Header("tenant-id", tenantId);
    }

    public static Header createTenantRoleHeader(String tenantRole) {
        return new Header("tenant-role", tenantRole);
    }


    public static RequestSpecification getBaseRequestSpecification() {
        return given()
                .relaxedHTTPSValidation()
                .log()
                .all();
    }
}
