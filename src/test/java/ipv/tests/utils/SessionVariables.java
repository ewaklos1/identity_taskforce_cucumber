package ipv.tests.utils;

public class SessionVariables {
    public static String TRANSACTION_ID = "transactionId";
    public static String ACCESS_TOKEN = "accessToken";
    public static String MOBILE_ID_URL = "mobileIdUrl";
    public static String MOBILE_ID_TOKEN = "mobileIdToken";
    public static String RESPONSE = "response";
    public static String DOCUMENT_ATTRIBUTES = "documentAttributes";
    public static String IDENTITY_ID = "identityId";
    public static String EVIDENCE_ID = "evidenceId";
}
