Feature: Identity deletion

  Scenario: Delete existing passport identity - imperative way
    When create identity
    Then valid response sent and identity created
    When create passport capture
    Then valid response sent and passport captured
    When delete passport identity
    Then valid response sent and passport deleted

  @test
  Scenario: Delete existing passport identity - declarative way
    Given Identity with passport exists
    When delete passport identity
    Then valid response sent and passport deleted