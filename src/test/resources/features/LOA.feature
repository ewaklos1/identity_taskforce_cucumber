Feature: LOA

  Scenario: Calculate identity score based on passport and selfie
    When create identity
    Then valid response sent and identity created
    When create passport capture
    Then valid response sent and passport captured
    When submit selfie
    When submit identity attributes
    When get identity
    Then identity contains declared attributes
    And identity score has been calculated