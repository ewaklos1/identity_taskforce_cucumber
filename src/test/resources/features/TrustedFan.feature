Feature: Trusted Fan

  Scenario: Trusted fan document submission
    When create transaction for TRUSTED_FAN business process
    And submit email
    Then EMAIL_INFO phase is ACCEPTED
    When submit phone number
    Then PHONE_INFO phase is ACCEPTED
    When submit phone otp
    Then PHONE_OTP phase is ACCEPTED
    When submit license
    Then LICENSE_DATA phase is ACCEPTED
    When submit document
    Then DOC_AUTH phase is ACCEPTED
    When submit face match
    Then FACE_MATCHING phase is ACCEPTED
    When submit additional attributes
    Then ADDITIONAL_ATTRIBUTES phase is ACCEPTED
    When submit issuance
    Then MOBILE_ID_ISSUANCE phase is ACCEPTED
    When submit end registration
    Then global status is OK and END_REGISTRATION phase is ACCEPTED
    When get transaction details
    Then valid response with mobileIdUrl and mobileIdToken is sent
    And retrieve mID
    Then mID contains declared attributes